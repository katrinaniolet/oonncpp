#include "adaline.h"
#include <cstdlib>
#include <fstream>
#include <iostream>
#include <iomanip>

int main( int argc, char* argv[] )
{
    UNUSED(argc)
    UNUSED(argv)

    srand(1);

    Pattern * data[250];
    std::ifstream infile("lin2var.trn");

    if(infile.fail()) {
        std::cout << "Unable to open pattern file..." << std::endl;
        exit(0);
    }

    for(int i=0; i < 250; ++i) {
        data[i] = new Pattern(2,1,infile);
        data[i]->print();
    }
    infile.close();

    BaseNode * node[4];
    BaseLink * link[3];

    node[0] = new InputNode;
    node[1] = new InputNode;
    node[2] = new BiasNode;
    node[3] = new AdalineNode(0.45);

    link[0] = new AdalineLink;
    link[1] = new AdalineLink;
    link[2] = new AdalineLink;

    connect(node[0],node[3],link[0]);
    connect(node[1],node[3],link[1]);
    connect(node[2],node[3],link[2]);

    int iteration = 0;
    int good = 0;

    while(good < 250) {
        good = 0;
        for(int i=0;i < 250;++i) {
            node[0]->setValue(data[i]->in(0));
            node[1]->setValue(data[i]->in(1));
            node[3]->run();
            if(data[i]->out(0) != node[3]->value()) {
                node[3]->learn();
                break;
            }
            else
                ++good;
        }
        std::cout << iteration << ".  " << good << "/250" << std::endl;
        ++iteration;
    }

    std::ofstream outfile("adaline1.net");

    for(int i = 0;i<4;++i)
        node[i]->save(outfile);
    for(int i = 0;i<3;++i)
        link[i]->save(outfile);
    outfile.close();

    for(int i = 0;i<4;++i)
        delete node[i];
    for(int i = 0;i<3;++i)
        delete link[i];


    node[0] = new InputNode;
    node[1] = new InputNode;
    node[2] = new BiasNode;
    node[3] = new AdalineNode;

    link[0] = new AdalineLink;
    link[1] = new AdalineLink;
    link[2] = new AdalineLink;

    connect(node[0],node[3],link[0]);
    connect(node[1],node[3],link[1]);
    connect(node[2],node[3],link[2]);

    infile.open("adaline1.net");
    for(int i=0;i<4;++i)
        node[i]->load(infile);
    for(int i=0;i<3;++i)
        link[i]->load(infile);
    infile.close();

    for(int i=0;i<250;++i) {
        node[0]->setValue(data[i]->in(0));
        node[1]->setValue(data[i]->in(1));

        node[3]->run();

        std::cout << "Pattern: " << std::setw(3) << i << "  Input: ("
                  << data[i]->in(0) << ","
                  << data[i]->in(1) << ")  ADALINE:"
                  << std::setw(3) << node[3]->value() << " Actual:"
                  << std::setw(3) << data[i]->out(0) << std::endl;
    }

    for(int i=0;i<4;++i)
        delete node[i];
    for(int i=0;i<3;++i)
        delete link[i];
}
