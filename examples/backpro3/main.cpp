#include <iostream>
#include <fstream>
#include <cstdlib>
#include <cassert>
#include <cmath>
#include "backpropagationnetwork.h"

#define MAX_PATTERNS 1000

enum State {
    Run,
    Learn
};

int main(int argc, char *argv[])
{
    if(argc < 2) {
        std::cout << "Usage: " << argv[0] << std::endl;
        std::cout << "Learning:" << std::endl;
        std::cout << "\tLEARN <pattern file> <network file>" << std::endl;
        std::cout << "\t<learning rate> <momentum>" << std::endl;
        std::cout << "\t<tolerance> <display> <input nodes>" << std::endl;
        std::cout << "\t|<middle nodes>| <output nodes>" << std::endl;
        std::cout << std::endl;
        std::cout << "Running:" << std::endl;
        std::cout << "RUN <pattern file> <network file>" << std::endl;
        exit(0);
    }

    std::cout << "Mode\t\t" << argv[1] << std::endl;
    std::cout << "Pattern File\t" << argv[2] << std::endl;
    std::cout << "Network File\t" << argv[3] << std::endl;

    State mode;
    double learningRate;
    double momentum;
    double tolerance;
    int displayInterval;
    int numLayers;
    int inputNodes;
    int outputNodes;
    int * nodeCount;

    if(toupper(argv[1][0]) == 'R') {
        mode = Run;
        if(argc < 4) {
            std::cout << "Invalid number of parameters for run mode." << std::endl;
            exit(-1);
        }
    }
    else if(toupper(argv[1][0]) == 'L') {
        mode = Learn;
        if(argc < 10) {
            std::cout << "Invalid number of parameters for learn mode." << std::endl;
            exit(-1);
        }
        learningRate = atof(argv[4]);
        momentum = atof(argv[5]);
        tolerance = atof(argv[6]);
        displayInterval = atoi(argv[7]);

        numLayers = argc - 8;
        nodeCount = new int[numLayers];
        for(int i=0;i<argc;++i) {
            nodeCount[i-8] = atoi(argv[i]);
        }
        std::cout << "Learning Rate:" << learningRate << std::endl;
        std::cout << "Momentum:" << momentum << std::endl;
        std::cout << "Tolerance:" << tolerance << std::endl;
        std::cout << "Display Interval:" << displayInterval << std::endl;

        outputNodes = nodeCount[numLayers-1];
        inputNodes = nodeCount[0];
        std::cout << "Layers:" << numLayers << std::endl;
        std::cout << "Input Node:" << nodeCount[0] << std::endl;
        for(int i = 1;i<numLayers-1;++i) {
            std::cout << "Middle Layer:" << nodeCount[i] << std::endl;
        }
        std::cout << "Output Node:" << nodeCount[numLayers-1] << std::endl;
        std::cout << std::endl;
    }
    else {
        std::cout << "Invalid mode" << std::endl;
        exit(-1);
    }

    if(mode == Learn) {
        Pattern * data[MAX_PATTERNS];
        int patternCount(0);

        std::ifstream infile(argv[2]);
        assert(infile);
        while(!infile.eof() && !infile.fail()) {
            if(patternCount >= MAX_PATTERNS) {
                std::cout << "Maximum number of training patterns exceeded." << std::endl;
                exit(-1);
            }
            data[patternCount] = new Pattern(inputNodes,outputNodes,infile);
            ++patternCount;
        }
        infile.close();

        for(int i=0;i<patternCount;++i) {
            data[i]->print();
        }

        BackpropagationNetwork BPNet(learningRate,momentum,numLayers,nodeCount);
        long iteration(0);
        int good(0);
        double totalError(0.0);
        int best(0);
        std::ofstream ofile("test.out");
        BPNet.print(ofile);
        ofile.close();

        while(good<patternCount) {
            good = 0;
            totalError = 0.0;
            for(int i=0;i<patternCount;++i) {
                BPNet.setValue(data[i]);
                BPNet.run();
                BPNet.setError(data[i]);
                BPNet.learn();
                int goodOutputs=0;
                for(int j=0;j<outputNodes;++j) {
                    if(fabs(BPNet.value(j) - data[i]->out(j) < tolerance))
                            ++goodOutputs;
                    totalError += fabs(BPNet.error());
                }
                if(goodOutputs == outputNodes)
                    ++good;
            }
            if(best < good) {
                best = good;
                std::ofstream outfile(argv[3]);
                BPNet.save(outfile);
                outfile.close();
                std::cout << "[Network Saved]" << std::endl;
            }
            //TODO(finish this one)
        }
    }
}
