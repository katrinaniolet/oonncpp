#include "backpropagation.h"
#include <cstdlib>
#include <cmath>
#include <iostream>
#include <iomanip>

int main( int argc, char * argv[] ) {
    UNUSED(argc)
    UNUSED(argv)

    srand(2);

     Pattern *data[16];
     data[0]=new Pattern(2,1,  1,   0.0,        0.0,        0.0 );
     data[1]=new Pattern(2,1,  2,   0.0,        0.2,        1.0 );
     data[2]=new Pattern(2,1,  3,   0.5,        0.0,        1.0 );
     data[3]=new Pattern(2,1,  4,   0.4,        0.3,        0.0 );
     data[4]=new Pattern(2,1,  5,   0.4,        0.4,        0.0 );
     data[5]=new Pattern(2,1,  6,   0.0,        0.8,        1.0 );
     data[6]=new Pattern(2,1,  7,   0.1,        0.0,        1.0 );
     data[7]=new Pattern(2,1,  8,   0.3,        1.0,        0.0 );
     data[8]=new Pattern(2,1,  9,   0.1,        0.1,        0.0 );
     data[9]=new Pattern(2,1,  10,  1.0,        0.0,        1.0 );
     data[10]=new Pattern(2,1, 11,  0.1,        0.0,        1.0 );
     data[11]=new Pattern(2,1, 12,  0.423232,   0.3,        0.0 );
     data[12]=new Pattern(2,1, 13,  0.1,        0.1,        0.0 );
     data[13]=new Pattern(2,1, 14,  0.0,        0.82312,    1.0 );
     data[14]=new Pattern(2,1, 15,  0.9837482,  0.0,        1.0 );
     data[15]=new Pattern(2,1, 16,  0.00001,    0.99999,    0.0 );

     BaseNode * node[6];
     BaseLink * link[9];

     node[0] = new InputNode;
     node[1] = new InputNode;
     node[2] = new BackpropagationMiddleNode( 0.4, 0.2 );
     node[3] = new BackpropagationMiddleNode( 0.4, 0.2 );
     node[4] = new BackpropagationMiddleNode( 0.4, 0.2 );
     node[5] = new BackpropagationOutputNode( 0.4, 0.2 );

     for(int i=0; i<9; ++i)
        link[i]=new BackpropagationLink();

     int curr = 0;
     for(int i=2; i<=4; ++i) {
        for(int j=0; j<=1; ++j) {
            connect(node[j],node[i],link[curr++]);
        }
     }

    for(int j=2; j<=4; ++j)
        connect(node[j],node[5],link[curr++]);


    long iteration = 0;
    int good = 0;
    double tolerance = 0.5;
    double totalError;

    while(good<16) {
        good = 0;
        totalError = 0.0;

        for (int i=0; i<16; ++i) {
            node[0]->setValue(data[i]->in(0));
            node[1]->setValue(data[i]->in(1));

            for(int j=2; j<=5; ++j) {
                node[j]->run();
            }

            node[5]->setError(data[i]->out(0));

            for(int j=5; j>=2; --j) {
                node[j]->learn();
            }

            if(fabs(node[5]->value()-data[i]->out(0))<tolerance)
                ++good;

            totalError += fabs(node[5]->error());
        }

        if(iteration%1000==0) {
            std::cout << iteration << ".   " << good << "/16"
                << "   Error: " << std::setprecision(15) << totalError << std::endl;
        }
        ++iteration;
    }

    std::ofstream outfile("bp1.net");
    for(int i=0; i<6; ++i)
        node[i]->save(outfile);

    for(int i=0; i<9; ++i)
        link[i]->save(outfile);

    outfile.close();

    for(int i=0; i<6; ++i)
        delete node[i];
    for(int i=0; i<9; ++i)
         delete link[i];

    node[0]=new InputNode;
    node[1]=new InputNode;
    node[2]=new BackpropagationNode;
    node[3]=new BackpropagationNode;
    node[4]=new BackpropagationNode;
    node[5]=new BackpropagationNode;

    for(int i=0; i<9; ++i)
        link[i] = new BackpropagationLink;

    curr = 0;
    for(int i=2; i<=4; ++i) {
        for(int j=0; j<=1; ++j) {
            connect(node[j],node[i],link[curr++]);
        }
    }

    for(int j=2; j<=4; ++j) {
        connect(node[j],node[5],link[curr++]);
    }


    std::ifstream infile("bp1.net");
    for(int i=0; i<6; ++i)
        node[i]->load(infile);

    for(int i=0; i<9; ++i)
        link[i]->load(infile);

    infile.close();

    for(int i=0; i<16; ++i) {
        node[0]->setValue(data[i]->in(0));
        node[1]->setValue(data[i]->in(1));

        for(int j=2; j<=5; ++j) {
            node[j]->run();
        }

        double out = node[5]->value();

        std::cout << "Pattern: " << std::setw(3) << i << "  Input: ("
            << data[i]->in(0) << ","
            << data[i]->in(1)
            << ")   Backprop: (" << out
            << ")    Actual: (" << data[i]->out(0) << ")" << std::endl;
    }

    for(int i=0; i<6; ++i)
        delete node[i];
    for(int i=0; i<9; ++i)
        delete link[i];
}
