#include "adaline.h"
#include <fstream>
#include <cstdlib>
#include <iostream>
#include <iomanip>

int main( int argc, char * argv[] )
{
    UNUSED(argc)
    UNUSED(argv)

    srand(1);

    Pattern * data[1000];
    std::ifstream infile("lin2var.trn");

    if(infile.fail()) {
        std::cerr << "Could not open pattern file..." << std::endl;
        exit(-1);
    }

    for(int i=0;i<1000;++i)
        data[i] = new Pattern(2,1,infile);
    infile.close();

    AdalineNetwork a(2,0.45);

    int iteration = 0;
    int good = 0;

    while(good < 1000 )
    {
        for(int i=0;i<1000;++i) {
            a.setValue(data[i]);
            a.run();
            if(data[i]->out(0) != a.value()) {
                a.learn();
                break;
            }
            else
                ++good;
        }
        std::cout << iteration << ".  " << good << "/1000" << std::endl;
        ++iteration;
    }

    std::ofstream outfile("test.dta");
    a.save(outfile);
    outfile.close();

    AdalineNetwork b("test.dta");

    int correct = 0;
    for(int i = 0;i<1000;++i)
    {
        b.setValue(data[i]);
        b.run();

        if(b.value() == data[i]->out(0))
            ++correct;
        else {
            std::cout << std::setw(3) << i << " Net: " << std::setprecision(7)
                      << b.value() << "  Actual: " << std::setprecision(7)
                      << data[i]->out(0) << std::endl;
        }

    }
    std:: cout << (int)((correct/1000.0)*100) << "% correct" << std::endl;

    return 0;
}
