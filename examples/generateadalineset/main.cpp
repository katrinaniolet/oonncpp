#include "defines.h"
#include <fstream>
#include <cstdlib>

int main( int argc, char* argv[] )
{
    UNUSED(argc)
    UNUSED(argv)

    srand(1);

    double x;
    double y;
    double y1;
    double output;

    std::ofstream outfile("lin2var.trn");

    for(int i = 0; i < 250; ++i) {
        x = ((double)rand()/(double)RAND_MAX)*2.0-1.0;
        y = ((double)rand()/(double)RAND_MAX)*2.0-1.0;

        y1 = (-5.0*x-2.0)/-4.0;

        if(y<y1)
            output = 1;
        else
            output = -1;

        outfile << i << " " << x << " " << y << " " << output << std::endl;
    }
    outfile.close();
}
