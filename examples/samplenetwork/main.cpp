#include "base.h"
#include <fstream>

int main( int argc, char * argv[] )
{
    UNUSED(argc)
    UNUSED(argv)

    BaseNode node[6];
    BaseLink link[8];

    connect(node[0],node[2],link[0]);
    connect(node[0],node[3],link[1]);
    connect(node[1],node[2],link[2]);
    connect(node[1],node[3],link[3]);
    connect(node[2],node[4],link[4]);
    connect(node[2],node[5],link[5]);
    connect(node[3],node[4],link[6]);
    connect(node[3],node[5],link[7]);

    std::ofstream outfile("Output.dta");

    for(int i=0;i<6;++i)
        node[i].print(outfile);

    outfile.close();

    return 0;
}
