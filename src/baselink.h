#ifndef BASELINK_H
#define BASELINK_H

#include <fstream>
#include "defines.h"

class BaseNode;

class BaseLink
{
public:
    BaseLink( int size=1 );
    virtual ~BaseLink();

    int id() const;
    virtual const char * name() const;

    virtual int getSetSize() const;
    virtual void updateWeight( double newVal );

    virtual void save( std::ofstream & outfile );
    virtual void load( std::ifstream & infile );

    virtual double value( int id = WEIGHT ) const;
    virtual void setValue( double newVal, int id = WEIGHT );

    virtual BaseNode * inNode() const;
    virtual void setInNode( BaseNode * node, int id );

    virtual BaseNode * outNode() const;
    virtual void setOutNode( BaseNode * node, int id );

    virtual double inValue( int mode = NODE_VALUE ) const;
    virtual double outValue( int mode = NODE_VALUE ) const;
    virtual double inError( int mode = NODE_ERROR ) const;
    virtual double outError( int mode = NODE_ERROR ) const;
    virtual double weightedInValue( int mode = NODE_VALUE ) const;
    virtual double weightedOutValue( int mode = NODE_VALUE ) const;
    virtual double weightedInError( int mode = NODE_VALUE ) const;
    virtual double weightedOutError( int mode = NODE_VALUE ) const;

    virtual void epoch( int mode = 0 );

protected:
     int _id;
     double * _value;
     BaseNode * _inNode;
     BaseNode * _outNode;
     int _valueSize;

private:
     static int _ticket;

};

#endif // BASELINK_H
