#include <iostream>
#include "llist.h"
#include "baselink.h"

LList::LList() :
    _head(0),
    _tail(0),
    _curr(0),
    _count(0)
{
}

LList::~LList()
{
    clear();
}

bool LList::addToTail(BaseLink *element)
{
    _curr=0;
    return addNode( element );
}

bool LList::addNode(BaseLink *element)
{
    Node * temp = new Node;
    if(!temp) {
        std::cerr << "Unable to allocate Node..." << std::endl;
    }

    temp->element = element;

    if(!temp)
        return false;

    if(!_curr) {
        temp->prev =_tail;
        temp->next = 0;
        if(!_tail) {
            _head = temp;
            _tail = temp;
        }
        else {
            _tail->next = temp;
            _tail = temp;
        }
    }
    else if(_curr == _head) {
        temp->prev = 0;
        temp->next = _head;
        if(!_head) {
            _head = temp;
            _tail = temp;
        }
        else {
            _head->prev = temp;
            _head = temp;
        }
    }
    else {
        temp->prev = _curr->prev;
        temp->next = _curr;
        _curr->prev->next = temp;
        _curr->prev = temp;
    }
    ++_count;
    return true;
}

bool LList::delNode()
{
    if(!_curr)
        return false;

    delete _curr->element;

    if(_curr == _head) {
        if( _head == _tail)
            _tail = 0;
        else
            _head->next->prev = 0;
        _head = _curr->next;
    }
    else if(_curr == _tail) {
        _tail->prev->next = 0;
        _tail = _curr->prev;
    }
    else {
        _curr->next->prev = _curr->prev;
        _curr->prev->next = _curr->next;
    }

    delete _curr;
    _curr = 0;
    --_count;
    return true;
}

bool LList::del(BaseLink *element)
{
    if(!find(element))
        return false;
    return delNode();
}

int LList::find(BaseLink *element)
{
    Node * temp = _head;
    int cnt = 1;
    _curr = 0;
    while(temp) {
        if(temp->element == element) {
            _curr = temp;
            return cnt;
        }
        ++cnt;
        temp = temp->next;
    }
    return 0;
}

int LList::count() const
{
    return _count;
}

BaseLink *LList::current() const
{
    if(!_curr)
        return 0;
    return _curr->element;
}

void LList::clear()
{
    Node * i = _head;
    Node * temp;

    while(i) {
        temp = i;
        i = i->next;
        delete temp;
    }
    _curr = 0;
    _head = 0;
    _tail = 0;
    _count=0;
}

void LList::resetToHead()
{
    _curr = _head;
}

void LList::resetToTail()
{
    _curr = _tail;
}

void LList::next()
{
    if(_curr->next == 0)
        _curr = _head;
    else
        _curr = _curr->next;
}

void LList::prev()
{
    if(_curr->prev == 0)
        _curr = _tail;
    else
        _curr = _curr->prev;
}
