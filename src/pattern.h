#ifndef PATTERN_H
#define PATTERN_H

#include <fstream>

class Pattern
{
public:
    Pattern( int in, int out );
    Pattern( int in, int out, int dataId, ... );
    Pattern( int in, int out, int dataId, double * inArray, double * outArray );
    Pattern( int in, int out, std::ifstream & infile );

    virtual ~Pattern();

    virtual int id() const;

    virtual double in(int id) const;
    virtual double out(int id) const;

    virtual void setIn( int id, double value );
    virtual void setOut( int id, double value );

    virtual int inSize() const;
    virtual int outSize() const;

    virtual void save( std::ofstream & outfile );
    virtual void load( std::ifstream & infile );

    virtual void print();

    virtual void copy( Pattern & in );

private:
    double * _inSet;
    double * _outSet;
    int _id;
    int _inSize;
    int _outSize;
};

#endif // PATTERN_H
