#include "basenode.h"
#include "baselink.h"
#include "llist.h"
#include <cstdlib>

void connect( BaseNode & fromNode, BaseNode & toNode, BaseLink * link )
{
    fromNode.createLinkTo(toNode,link);
}

void connect(BaseNode & fromNode, BaseNode & toNode, BaseLink & link )
{
    fromNode.createLinkTo(toNode,&link);
}

void connect(BaseNode * fromNode, BaseNode * toNode, BaseLink * link )
{
    fromNode->createLinkTo(*toNode,link);
}

bool disconnect( BaseNode * fromNode, BaseNode * toNode )
{
    LList * outLinks = fromNode->outLinks();
    int flag = 0;
    outLinks->resetToHead();
    for(int i=0; i<outLinks->count(); ++i) {
        if(outLinks->current()->outNode() == toNode) {
            flag = 1;
            break;
        }
        outLinks->next();
    }

    if(flag==1) {
        outLinks->current()->outNode()->inLinks()->del(outLinks->current());
        outLinks->delNode();
        return true;
    }
    else
        return false;
}

double random( double lowerBound, double upperBound )
{
    return ((double)((rand()%RAND_MAX))/(double)RAND_MAX)*(upperBound-lowerBound)+lowerBound;
}

