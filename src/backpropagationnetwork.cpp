#include "backpropagationnetwork.h"
#include "backpropagationmiddlenode.h"
#include "backpropagationlink.h"
#include <iostream>
#include <cstdarg>

BackpropagationNetwork::BackpropagationNetwork(const char *filename) :
    AdalineNetwork(),
    _nodeCnt(0)
{
    std::ifstream infile(filename);
    load(infile);
    infile.close();
}

BackpropagationNetwork::BackpropagationNetwork(double lr, double mt, int layers, ...) :
    AdalineNetwork()
{
    _numNodes = 0;
    _numLinks = 0;

    _numLayers = layers;
    va_list vl;
    va_start(vl, layers );

    _nodeCnt = new int[layers];

    for(int i=0; i<layers; ++i) {
        _nodeCnt[i]=va_arg(vl,int);
        _numNodes += _nodeCnt[i];
        if(i>0)
            _numLinks += _nodeCnt[i-1]*_nodeCnt[i];
    }
    va_end( vl );

    _learningRate=lr;
    _momentumTerm = mt;
    createNetwork();
}

BackpropagationNetwork::BackpropagationNetwork(double lr, double mt, int layers, int nodes[]) :
    AdalineNetwork(),
    _numLayers(layers),
    _nodeCnt(new int[layers]),
    _momentumTerm(mt)
{
    _numNodes = 0;
    _numLinks = 0;
    _learningRate = lr;
    for(int i=0; i<layers; ++i) {
        _nodeCnt[i] = nodes[i];
        _numNodes += _nodeCnt[i];
        if(i>0)
            _numLinks += _nodeCnt[i-1]*_nodeCnt[i];
    }
    createNetwork();
}

BackpropagationNetwork::BackpropagationNetwork() :
    AdalineNetwork(),
    _nodeCnt(0)
{
}

BackpropagationNetwork::~BackpropagationNetwork()
{
    if(_nodeCnt)
        delete[] _nodeCnt;
}

double BackpropagationNetwork::error(int id) const
{
    return _node[id+_firstOutputNode]->error();
}

void BackpropagationNetwork::setError(double new_val, int id)
{
    _node[id+_firstOutputNode]->setError(new_val);
}

void BackpropagationNetwork::setError(Pattern *outputPattern)
{
    for(int i=0; i<_nodeCnt[_numLayers-1]; ++i)
        _node[i+_firstOutputNode]->setError(outputPattern->out(i));
}

double BackpropagationNetwork::value(int id) const
{
    return _node[id+_firstOutputNode]->value();
}

void BackpropagationNetwork::save(std::ofstream &outfile)
{
    outfile << _id <<  std::endl;
    outfile << _numLayers << std::endl;
    for(int i=0; i<_numLayers; ++i)
    outfile << _nodeCnt[i] << std::endl;
    saveNodesLinks( outfile );
}

void BackpropagationNetwork::load(std::ifstream &infile)
{
    infile >> _id;
    infile >> _numLayers;
    if(_nodeCnt)
        delete[] _nodeCnt;
    _nodeCnt = new int[_numLayers];
    for(int i=0; i<_numLayers; ++i)
        infile >> _nodeCnt[i];

    loadNodesLinks( infile );
}

void BackpropagationNetwork::run(int mode)
{
    UNUSED(mode)

    loadInputs();
    for(int i=_firstMiddleNode; i<_numNodes; ++i)
        _node[i]->run();
}

void BackpropagationNetwork::learn(int mode)
{
    UNUSED(mode)

    int cnt = _outLinks.count();
    if(cnt>0) {
            _outLinks.resetToHead();
        for(int i=0; i<cnt; ++i) {
            _node[i+_firstOutputNode]->setError(_outLinks.current()->outError());
            _outLinks.next();
        }
    }

    for(int i=_numNodes-1; i>=_firstMiddleNode; --i)
        _node[i]->learn();
}

const char *BackpropagationNetwork::name() const
{
    return "BACKPROP_NETWORK";
}

int BackpropagationNetwork::numLayers() const
{
    return _numLayers;
}

int BackpropagationNetwork::layerCount(int id) const
{
    return _nodeCnt[id];
}

void BackpropagationNetwork::createNetwork()
{
    _node = new BaseNode*[_numNodes];
    _link = new BaseLink*[_numLinks];
    int curr = 0;
    for(int i=0; i<_nodeCnt[0]; ++i)
         _node[curr++] = new InputNode;

    _firstMiddleNode = curr;
    for(int i=1; i<_numLayers-1; ++i) {
         for(int j=0; j<_nodeCnt[i]; ++j) {
              _node[curr++]=new BackpropagationMiddleNode(_learningRate,_momentumTerm);
         }
    }
    _firstOutputNode = curr;
    for(int i=0; i<_nodeCnt[_numLayers-1]; ++i)
         _node[curr++] = new BackpropagationOutputNode(_learningRate,_momentumTerm);

    for(int i=0; i<_numLinks; ++i) {
        _link[i] = new BackpropagationLink;
        if(_link[i] == 0)
            std::cerr << "Error - Failed to allocate link " << i << std::endl;
    }

    curr = 0;
    int layer1 = 0;
    int layer2 = _firstMiddleNode;

    for(int i=0; i<_numLayers-1; ++i) {
        for (int j=0; j<_nodeCnt[i+1]; ++j) {
            for(int k=0; k<_nodeCnt[i]; ++k) {
                connect(_node[layer1+k],_node[layer2+j],_link[curr++]);
            }
        }
        layer1 = layer2;
        layer2 += _nodeCnt[i+1];
    }
}
