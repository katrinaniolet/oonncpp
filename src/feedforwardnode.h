#ifndef FORWARDNODE_H
#define FORWARDNODE_H

#include "basenode.h"

class FeedForwardNode : public BaseNode
{
public:
     FeedForwardNode( int vSize=1, int eSize=1 );
     void run( int mode = 0 );
     const char * name() const;

protected:
     virtual double transferFunction( double value );

};

#endif // FORWARDNODE_H
