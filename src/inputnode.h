#ifndef INPUTNODE_H
#define INPUTNODE_H

#include "basenode.h"

class InputNode : public BaseNode
{
public:
    InputNode( int size=1 );

    virtual const char * name() const;
};

#endif // INPUTNODE_H
