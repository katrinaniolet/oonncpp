#ifndef BACKPROPAGATIONMIDDLENODE_H
#define BACKPROPAGATIONMIDDLENODE_H

#include "backpropagationoutputnode.h"

class BackpropagationMiddleNode : public BackpropagationOutputNode
{
public:
    BackpropagationMiddleNode( double lr, double mt, int vSize = 3, int eSize = 1 );
    const char * name() const;

protected:
    double computeError( int mode = 0 );
};

#endif // BACKPROPAGATIONMIDDLENODE_H
