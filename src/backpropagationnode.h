#ifndef BACKPROPAGATIONNODE_H
#define BACKPROPAGATIONNODE_H

#include "adaline.h"

class BackpropagationNode : public FeedForwardNode
{
public:
    BackpropagationNode( int vSize = 1, int eSize = 0 );
    const char * name() const;

protected:
    double transferFunction( double value );
};

#endif // BACKPROPAGATIONNODE_H
