#ifndef BIASNODE_H
#define BIASNODE_H

#include "inputnode.h"

class BiasNode : public InputNode
{
public:

    BiasNode( double bias=1.0 );
    void setValue( double value, int id=0 );
    double value( int id=0 ) const;
    const char * name() const;
};

#endif // BIASNODE_H
