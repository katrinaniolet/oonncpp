#include "feedforwardnode.h"
#include "baselink.h"

FeedForwardNode::FeedForwardNode( int vSize, int eSize ) :
    BaseNode(vSize,eSize)
{
}

void FeedForwardNode::run(int mode)
{
    _inLinks.resetToHead();
    double total = 0.0;
    int cnt = _inLinks.count();
    for(int i=0; i<cnt; ++i) {
        total += _inLinks.current()->weightedInValue();
        _inLinks.next();
    }
    _value[mode]=transferFunction(total);
}

const char *FeedForwardNode::name() const
{
    return "FEED_FORWARD_NODE";
}

double FeedForwardNode::transferFunction(double value)
{
    return value;
}
