#include <iomanip>
#include "baselink.h"
#include "basenode.h"

int BaseLink::_ticket=-1;

BaseLink::BaseLink( int size ) :
    _id(++_ticket),
    _value(0),
    _inNode(0),
    _outNode(0),
    _valueSize(size)
{
    if(size > 0)
        _value = new double[size];
    for(int i=0; i<size; ++i)
        _value[i] = 0.0;
}

BaseLink::~BaseLink()
{
    if(_valueSize>0)
        delete[] _value;
}

int BaseLink::id() const
{
    return _id;
}

const char *BaseLink::name() const
{
    return "BASE_LINK";
}

int BaseLink::getSetSize() const
{
    return _valueSize;
}

void BaseLink::updateWeight(double newVal)
{
    _value[WEIGHT] += newVal;
}

void BaseLink::save(std::ofstream &outfile)
{
    outfile << _id << std::endl;
    outfile << _valueSize;
//    if(value) delete []value;
//    value=new double[value_size];
    for(int i=0; i<_valueSize; ++i)
        outfile << " " << std::setprecision(18) << _value[i];
    outfile << std::endl;
}

void BaseLink::load(std::ifstream &infile)
{
    infile >> _id;
    infile >> _valueSize;
    if(_value)
        delete[] _value;
    _value = new double[_valueSize];
    for (int i=0; i<_valueSize; ++i)
          infile >> _value[i];
}

double BaseLink::value(int id) const
{
    return _value[id];
}

void BaseLink::setValue(double newVal, int id)
{
    _value[id] = newVal;
}

BaseNode *BaseLink::inNode() const
{
    return _inNode;
}

void BaseLink::setInNode(BaseNode *node, int id)
{
    UNUSED(id)
    _inNode = node;
}

BaseNode *BaseLink::outNode() const
{
    return _outNode;
}

void BaseLink::setOutNode(BaseNode *node, int id)
{
    UNUSED(id)
    _outNode = node;
}

double BaseLink::inValue(int mode) const
{
    return _inNode->value(mode);
}

double BaseLink::outValue(int mode) const
{
    return _outNode->value(mode);
}

double BaseLink::inError(int mode) const
{
    return _inNode->error(mode);
}

double BaseLink::outError(int mode) const
{
    return _outNode->error(mode);
}

double BaseLink::weightedInValue(int mode) const
{
    return _inNode->value(mode)*_value[WEIGHT];
}

double BaseLink::weightedOutValue(int mode) const
{
    return _outNode->value(mode)*_value[WEIGHT];
}

double BaseLink::weightedInError(int mode) const
{
    return _inNode->error(mode)*_value[WEIGHT];
}

double BaseLink::weightedOutError(int mode) const
{
    return _outNode->error(mode)*_value[WEIGHT];
}

void BaseLink::epoch(int mode)
{
    UNUSED(mode)
}
