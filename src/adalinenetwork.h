#ifndef ADALINENETWORK_H
#define ADALINENETWORK_H

#include "base.h"

class AdalineNetwork : public BaseNetwork
{
public:
    AdalineNetwork( const char * filename );
    AdalineNetwork( int size );
    AdalineNetwork( int size, double lr );
    AdalineNetwork();
    double value( int id = 0 ) const;
    void setValue( double newVal, int id = 0 );
    void setValue( Pattern * input_pattern );
    void save( std::ofstream & outfile );
    void load( std::ifstream & infile );
    void run( int mode=0 );
    void learn( int mode=0 );
    const char * name() const;

protected:
     void createNetwork();
     void loadInputs();

     double _learningRate;
};

#endif // ADALINENETWORK_H
