#include "backpropagationlink.h"
#include "defines.h"
#include <iomanip>

BackpropagationLink::BackpropagationLink(int size) :
    BaseLink(size)
{
    _value[WEIGHT] = random(-1.0,1.0);
    _value[DELTA] = 0.0;
}

void BackpropagationLink::save(std::ofstream &outfile)
{
    outfile << std::setw(4) << _id <<  " " << std::setprecision(18)
        << _value[WEIGHT] << " " << std::setw(4)
        << inNode()->id() << " "
        << std::setw(4) << outNode()->id() << std::endl;
}

void BackpropagationLink::load(std::ifstream &infile)
{
    infile >> _id;
    infile >> _value[WEIGHT];
    int dummy;
    infile >> dummy;
    infile >> dummy;
}

const char *BackpropagationLink::name() const
{
    return "BP_LINK";
}

void BackpropagationLink::updateWeight(double newVal)
{
    double momentum = outNode()->value( MOMENTUM );
    _value[WEIGHT] += newVal + ( momentum * _value[DELTA] );
    _value[DELTA] = newVal;
}
