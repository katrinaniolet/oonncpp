#ifndef BACKPROPAGATIONOUTPUTNODE_H
#define BACKPROPAGATIONOUTPUTNODE_H

#include "backpropagationnode.h"

class BackpropagationOutputNode : public BackpropagationNode
{
public:
    BackpropagationOutputNode( double lr, double mt, int vSize = 3, int eSize = 1 );

protected:
    virtual double computeError( int mode = 0 );
    void learn( int mode = 0 );
    const char * name() const;
};

#endif // BACKPROPAGATIONOUTPUTNODE_H
