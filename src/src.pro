TEMPLATE = lib
CONFIG += warn_on
DESTDIR = ../bin
TARGET = oonncpp
QT -= core gui

SOURCES += basenode.cpp \
    baselink.cpp \
    llist.cpp \
    feedforwardnode.cpp \
    basenetwork.cpp \
    inputnode.cpp \
    biasnode.cpp \
    pattern.cpp \
    functions.cpp \
    adalinenode.cpp \
    adalinelink.cpp \
    adalinenetwork.cpp \
    backpropagationnode.cpp \
    backpropagationlink.cpp \
    backpropagationoutputnode.cpp \
    backpropagationmiddlenode.cpp \
    backpropagationnetwork.cpp

HEADERS += \
    basenode.h \
    baselink.h \
    defines.h \
    llist.h \
    feedforwardnode.h \
    basenetwork.h \
    inputnode.h \
    biasnode.h \
    pattern.h \
    functions.h \
    base.h \
    adalinenode.h \
    adalinelink.h \
    adalinenetwork.h \
    adaline.h \
    backpropagationnode.h \
    backpropagationlink.h \
    backpropagationoutputnode.h \
    backpropagationmiddlenode.h \
    backpropagationnetwork.h \
    backpropagation.h
