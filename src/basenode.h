#ifndef BASENODE_H
#define BASENODE_H

#include "llist.h"
#include "defines.h"
#include <fstream>

class BaseNode
{
public:
    BaseNode( int vSize = 1, int eSize = 1 );
    virtual ~BaseNode();

    int id() const;
    virtual const char * name() const;

    LList * inLinks();
    LList * outLinks();

    virtual void run( int mode = 0 );
    virtual void learn( int mode = 0 );
    virtual void epoch( int code = 0 );
    virtual void load( std::ifstream & infile );
    virtual void save( std::ofstream & outfile);

    virtual double value( int id = NODE_VALUE ) const;
    virtual void setValue( double newVal, int id = NODE_VALUE );

    virtual double error( int id = NODE_ERROR ) const;
    virtual void setError( double newVal, int id = NODE_ERROR );

    void createLinkTo( BaseNode & to_node, BaseLink * link );
    virtual void print( std::ofstream & out );

    friend void connect( BaseNode & fromNode, BaseNode & toNode, BaseLink * link );
    friend void connect( BaseNode & fromNode, BaseNode & toNode, BaseLink & link );
    friend void connect( BaseNode * fromNode, BaseNode * toNode, BaseLink * link );
    friend bool disconnect( BaseNode * fromNode, BaseNode * toNode);

    friend double random( double lowerBound, double upperBound );

protected:
     int _id;
     double * _value;
     int _valueSize;
     double * _error;
     int _errorSize;

     LList _inLinks;
     LList _outLinks;

private:
     static int _ticket;

};

#endif // BASENODE_H
