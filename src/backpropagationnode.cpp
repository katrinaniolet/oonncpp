#include <cmath>
#include "backpropagationnode.h"

BackpropagationNode::BackpropagationNode( int vSize, int eSize ) :
    FeedForwardNode(vSize,eSize)
{
}

const char *BackpropagationNode::name() const
{
    return "BP_NODE";
}

double BackpropagationNode::transferFunction(double value)
{
    return (1.0/(1.0+exp(-value)));
}
