#include "backpropagationoutputnode.h"

BackpropagationOutputNode::BackpropagationOutputNode( double lr, double mt, int vSize, int eSize ) :
    BackpropagationNode(vSize,eSize)
{
    _value[LEARNING_RATE]=lr;
    _value[MOMENTUM]=mt;
}

double BackpropagationOutputNode::computeError(int mode)
{
    UNUSED(mode)

    return _value[NODE_VALUE]*(1.0-_value[NODE_VALUE]) * (_error[NODE_ERROR]-_value[NODE_VALUE]);
}

void BackpropagationOutputNode::learn(int mode)
{
    UNUSED(mode)

    double delta;
    _error[NODE_ERROR] = computeError();

    _inLinks.resetToHead();
    BaseLink * link;
    int cnt = _inLinks.count();

    for (int i=0; i<cnt; ++i) {
        link = _inLinks.current();
        delta = _value[LEARNING_RATE]*_error[NODE_ERROR]*link->inValue();
        link->updateWeight(delta);
        _inLinks.next();
    }
}

const char *BackpropagationOutputNode::name() const
{
    return "BP_OUTPUT_NODE";
}
