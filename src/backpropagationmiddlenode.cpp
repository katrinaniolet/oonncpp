#include "backpropagationmiddlenode.h"

BackpropagationMiddleNode::BackpropagationMiddleNode(  double lr, double mt, int vSize, int eSize ) :
    BackpropagationOutputNode(lr,mt,vSize, eSize)
{
}

const char *BackpropagationMiddleNode::name() const
{
    return "BP_MIDDLE_NODE";
}

double BackpropagationMiddleNode::computeError(int mode)
{
    UNUSED(mode)

    double total = 0.0;
    _outLinks.resetToHead();
    int cnt = _outLinks.count();
    for(int i=0; i<cnt; ++i) {
        total += _outLinks.current()->weightedOutError();
        _outLinks.next();
    }
    return _value[NODE_VALUE]*(1.0-_value[NODE_VALUE])*total;
}
