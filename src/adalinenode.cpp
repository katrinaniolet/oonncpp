#include "adalinenode.h"

AdalineNode::AdalineNode() :
    FeedForwardNode(2,1)
{
}

AdalineNode::AdalineNode(double lr) :
    FeedForwardNode(2,1)
{
    _value[LEARNING_RATE] = lr;
}

void AdalineNode::learn(int mode)
{
    UNUSED(mode)
    _error[NODE_ERROR] = _value[NODE_VALUE]*-2.0;
    BaseLink * link;
    _inLinks.resetToHead();
    int cnt = _inLinks.count();
    double delta;
    for(int i=0; i<cnt; ++i) {
        link = _inLinks.current();
        delta = _value[LEARNING_RATE]*link->inValue()*_error[NODE_ERROR];
        link->updateWeight(delta);
        _inLinks.next();
    }
}

const char *AdalineNode::name() const
{
    return "ADALINE_NODE";
}

double AdalineNode::transferFunction(double value)
{
    if(value<0)
        return -1.0;
    else
        return 1.0;
}
