#ifndef BASENETWORK_H
#define BASENETWORK_H

#include "basenode.h"

class BaseNetwork : public BaseNode
{
public:
    BaseNetwork();
    ~BaseNetwork();

    void epoch( int code=0 );
    void print( std::ofstream &outfile );
    const char * name() const;

protected:
    virtual void createNetwork();
    virtual void loadInputs();
    virtual void saveNodesLinks( std::ofstream & outfile );
    virtual void loadNodesLinks( std::ifstream & infile );

    int _numNodes;
    int _numLinks;
    BaseNode **_node;
    BaseLink **_link;
};

#endif // BASENETWORK_H
