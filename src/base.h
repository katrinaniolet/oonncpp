#ifndef BASE_H
#define BASE_H

#include "defines.h"
#include "baselink.h"
#include "basenode.h"
#include "basenetwork.h"
#include "biasnode.h"
#include "feedforwardnode.h"
#include "functions.h"
#include "inputnode.h"
#include "llist.h"
#include "pattern.h"

#endif // BASE_H
