#ifndef ADALINELINK_H
#define ADALINELINK_H

#include "base.h"

class AdalineLink : public BaseLink
{
public:
    AdalineLink();
    void save( std::ofstream & outfile );
    void load( std::ifstream & infile );
    const char * name() const;
};

#endif // ADALINELINK_H
