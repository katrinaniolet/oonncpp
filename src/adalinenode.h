#ifndef ADALINENODE_H
#define ADALINENODE_H

#include "base.h"

class AdalineNode : public FeedForwardNode
{
public:
     AdalineNode();
     AdalineNode( double lr );
     void learn( int mode );
     const char * name() const;

protected:
     double transferFunction( double value );
};

#endif // ADALINENODE_H
