#ifndef BACKPROPAGATIONLINK_H
#define BACKPROPAGATIONLINK_H

#include "adaline.h"

class BackpropagationLink : public BaseLink
{
public:
    BackpropagationLink( int size = 2 );

    void save( std::ofstream & outfile );
    void load( std::ifstream & infile );
    const char * name() const;
    void updateWeight( double newVal );

};

#endif // BACKPROPAGATIONLINK_H
