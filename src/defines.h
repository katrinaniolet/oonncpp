#ifndef DEFINES_H
#define DEFINES_H

#define NODE_VALUE 0
#define LEARNING_RATE 1
#define NODE_ERROR 0
#define WEIGHT 0

#define DELTA 1
#define MOMENTUM 2

#define UNUSED(var) ((void)var);

#endif // DEFINES_H
