#include "adalinelink.h"
#include <iomanip>

AdalineLink::AdalineLink() :
    BaseLink()
{
    _value[WEIGHT] = random(-1.0,1.0);
}

void AdalineLink::save(std::ofstream &outfile)
{
    outfile << std::setw(4) << _id <<  " " << std::setprecision(18)
        << _value[WEIGHT] << " " << std::setw(4) << inNode()->id() << " "
        << std::setw(4) << outNode()->id() << std::endl;
}

void AdalineLink::load(std::ifstream &infile)
{
    infile >> _id;
    infile >> _value[WEIGHT];
    int dummy;
    infile >> dummy;   // Skip over node IDs
    infile >> dummy;
}

const char *AdalineLink::name() const
{
    return "ADALINE_LINK";
}
