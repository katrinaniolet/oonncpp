#include <cstdarg>
#include <iostream>
#include "pattern.h"

Pattern::Pattern( int in, int out ) :
    _inSet(new double[in]),
    _outSet(new double[out]),
    _id(-1),
    _inSize(in),
    _outSize(out)
{
}

Pattern::Pattern( int in, int out, int dataId, ... ) :
    _inSet(new double[in]),
    _outSet(new double[out]),
    _id(dataId),
    _inSize(in),
    _outSize(out)
{
    va_list vl;
    va_start(vl, dataId );
    for (int i=0; i<in; ++i)
           _inSet[i]=va_arg(vl,double);

    for (int i=0; i<out; ++i)
           _outSet[i]=va_arg(vl,double);

    va_end( vl );
}

Pattern::Pattern( int in, int out, int dataId, double * inArray, double * outArray ) :
    _inSet(new double[in]),
    _outSet(new double[out]),
    _id(dataId),
    _inSize(in),
    _outSize(out)
{
    for(int i=0; i<in; ++i)
        _inSet[i] = inArray[i];

    for(int i=0; i<out; ++i)
        _outSet[i]= outArray[i];

}

Pattern::Pattern( int in, int out, std::ifstream & infile ) :
    _inSet(new double[in]),
    _outSet(new double[out]),
    _id(-1),
    _inSize(in),
    _outSize(out)
{
    infile >> _id;
    load(infile);
}

Pattern::~Pattern()
{
    if(_inSet)
        delete[] _inSet;
    if(_outSet)
        delete[] _outSet;
}

int Pattern::id() const
{
    return _id;
}

double Pattern::in(int id) const
{
    return _inSet[id];
}

double Pattern::out(int id) const
{
    return _outSet[id];
}

void Pattern::setIn(int id, double value)
{
    _inSet[id] = value;
}

void Pattern::setOut(int id, double value)
{
    _outSet[id] = value;
}

int Pattern::inSize() const
{
    return _inSize;
}

int Pattern::outSize() const
{
    return _outSize;
}

void Pattern::save(std::ofstream &outfile)
{
    outfile << _id << "\t";
    for(int i=0; i<_inSize; ++i)
        outfile << _inSet[i] << "\t";

    for(int i=0; i<_outSize; ++i) {
        outfile << _outSet[i];
        if(i!=_outSize-1)
            outfile << '\t';
    }
    outfile << std::endl;
}

void Pattern::load(std::ifstream &infile)
{
    for(int i=0; i<_inSize; ++i)
         infile >> _inSet[i];

    for(int i=0; i<_outSize; ++i)
         infile >> _outSet[i];

    char ch;
    ch = infile.peek();
    while(ch=='\n' || ch==EOF) {
        ch=infile.get();
        if(ch == EOF)
            break;
        ch = infile.peek();
    }
}

void Pattern::print()
{
    std::cout << "ID: " << _id << "   In: ";
    for(int i=0; i<_inSize; ++i)
        std::cout << _inSet[i] << " ";
    std::cout << "   Out: ";
    for(int i=0; i<_outSize; ++i)
        std::cout << _outSet[i] << " ";
    std::cout << std::endl;
}

void Pattern::copy(Pattern &orig)
{
    if(orig.inSize() == _inSize)
        for(int i=0; i<_inSize; ++i)
            _inSet[i] = orig.in(i);

    if(orig.outSize() == _outSize)
        for(int i=0; i<_outSize; ++i)
            _outSet[i] = orig.out(i);
}
