#ifndef FUNCTIONS_H
#define FUNCTIONS_H

class BaseNode;
class BaseLink;

void connect( BaseNode & fromNode, BaseNode & toNode, BaseLink * link );
void connect(BaseNode & fromNode, BaseNode & toNode, BaseLink & link );
void connect(BaseNode * fromNode, BaseNode * toNode, BaseLink * link );
bool disconnect( BaseNode * fromNode, BaseNode * toNode );
double random( double lowerBound, double upperBound );

#endif // FUNCTIONS_H
