#include "adalinenode.h"
#include "adalinelink.h"
#include "adalinenetwork.h"

AdalineNetwork::AdalineNetwork(const char *filename) :
    BaseNetwork()
{
    std::ifstream infile(filename);
    load(infile);
    infile.close();
}

AdalineNetwork::AdalineNetwork(int size) :
    BaseNetwork()
{
    _numNodes = size+2;
    _numLinks = size+1;
    _learningRate = 0;
    createNetwork();
}

AdalineNetwork::AdalineNetwork(int size, double lr) :
    BaseNetwork()
{
    _numNodes = size+2;
    _numLinks = size+1;
    _learningRate = lr;
    createNetwork();
}

AdalineNetwork::AdalineNetwork() :
    BaseNetwork()
{
}

double AdalineNetwork::value(int id) const
{
    UNUSED(id)
    return _node[_numNodes-1]->value();
}

void AdalineNetwork::setValue(double newVal, int id)
{
    _node[id]->setValue(newVal);
}

void AdalineNetwork::setValue(Pattern *inputPattern)
{
    for(int i=0; i<inputPattern->inSize(); ++i)
        _node[i]->setValue(inputPattern->in(i));
}

void AdalineNetwork::save(std::ofstream &outfile)
{
    outfile << _id <<  std::endl;
    saveNodesLinks( outfile );
}

void AdalineNetwork::load(std::ifstream &infile)
{
    infile >> _id;
    loadNodesLinks( infile );
}

void AdalineNetwork::run(int mode)
{
    UNUSED(mode)
    loadInputs();
    _node[_numNodes-1]->run();
}

void AdalineNetwork::learn(int mode)
{
    UNUSED(mode)
    _node[_numNodes-1]->learn();
}

const char *AdalineNetwork::name() const
{
    return "ADALINE_Network";
}

void AdalineNetwork::createNetwork()
{
    _node = new BaseNode*[_numNodes];
    _link = new BaseLink*[_numLinks];

    for(int i=0; i<_numNodes-2; ++i)
        _node[i] = new InputNode;

    _node[_numNodes-2] = new BiasNode;

    _node[_numNodes-1] = new AdalineNode(_learningRate);

    for(int i=0; i<_numLinks; ++i)
         _link[i] = new AdalineLink;

    for(int i=0; i<_numLinks; ++i)
        connect(_node[i],_node[_numNodes-1],_link[i]);
}

void AdalineNetwork::loadInputs()
{
    int cnt = _inLinks.count();
    if(cnt>0) {
        _inLinks.resetToHead();
        for(int i=0; i<cnt; ++i) {
            setValue(_inLinks.current()->inValue(),i);
              _inLinks.next();
        }
    }
}
