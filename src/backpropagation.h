#ifndef BACKPROPAGATION_H
#define BACKPROPAGATION_H

#include "adaline.h"
#include "backpropagationlink.h"
#include "backpropagationmiddlenode.h"
#include "backpropagationnetwork.h"
#include "backpropagationnode.h"
#include "backpropagationoutputnode.h"

#endif // BACKPROPAGATION_H
