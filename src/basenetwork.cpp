#include "basenetwork.h"
#include "baselink.h"

BaseNetwork::BaseNetwork() :
    BaseNode(0,0),
    _numNodes(0),
    _numLinks(0),
    _node(0),
    _link(0)
{
}

BaseNetwork::~BaseNetwork()
{
    if(_node) {
        for(int i=0; i<_numNodes; ++i)
            delete _node[i];
        for(int i=0; i<_numLinks; ++i)
            delete _link[i];
        delete[] _node;
        delete[] _link;
    }
}

void BaseNetwork::epoch(int code)
{
    for(int i=0; i<_numNodes; ++i)
        _node[i]->epoch( code );

    for(int i=0; i<_numLinks; ++i)
        _link[i]->epoch( code );
}

void BaseNetwork::print(std::ofstream &outfile)
{
    for(int i=0; i<_numNodes; ++i)
        _node[i]->print(outfile);

}

const char *BaseNetwork::name() const
{
    return "BASE_NETWORK";
}

void BaseNetwork::createNetwork()
{
}

void BaseNetwork::loadInputs()
{
}

void BaseNetwork::saveNodesLinks(std::ofstream &outfile)
{
    outfile << _numNodes << std::endl;
    outfile << _numLinks << std::endl;
    for(int i=0; i<_numNodes; ++i)
        _node[i]->save(outfile);
    for(int i=0; i<_numLinks; ++i)
        _link[i]->save(outfile);
}

void BaseNetwork::loadNodesLinks(std::ifstream &infile)
{
    infile >> _numNodes;
    infile >> _numLinks;
    createNetwork();
    for(int i=0; i<_numNodes; ++i)
        _node[i]->load(infile);
    for(int i=0; i<_numLinks; ++i)
        _link[i]->load(infile);
}
