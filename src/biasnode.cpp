#include "biasnode.h"

BiasNode::BiasNode( double bias) :
    InputNode(1)
{
    _value[0]=bias;
}

void BiasNode::setValue(double value, int id)
{
    UNUSED(value)
    UNUSED(id)
}

double BiasNode::value(int id) const
{
    UNUSED(id)
    return _value[0];
}

const char *BiasNode::name() const
{
    return "BIAS_NODE";
}
