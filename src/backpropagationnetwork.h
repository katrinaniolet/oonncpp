#ifndef BACKPROPAGATIONNETWORK_H
#define BACKPROPAGATIONNETWORK_H

#include "adalinenetwork.h"

class BackpropagationNetwork : public AdalineNetwork
{
public:
    BackpropagationNetwork( const char * filename );
    BackpropagationNetwork( double lr, double mt, int layers, ... );
    BackpropagationNetwork( double lr, double mt, int layers, int nodes[] );
    BackpropagationNetwork();
    ~BackpropagationNetwork();

    double error( int id = 0 ) const;
    void setError( double new_val, int id = 0 );
    void setError(Pattern * outputPattern );
    double value( int id = 0 ) const;

    void save( std::ofstream & outfile );
    void load( std::ifstream & infile );

    void run( int mode = 0 );
    void learn( int mode = 0 );
    const char * name() const;

    virtual int numLayers() const;
    virtual int layerCount( int id ) const;

protected:
    void createNetwork();

     int _numLayers;
     int _firstMiddleNode;
     int _firstOutputNode;
     int * _nodeCnt;
     double _momentumTerm;

};

#endif // BACKPROPAGATIONNETWORK_H
