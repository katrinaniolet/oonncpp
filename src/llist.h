#ifndef LLIST_H
#define LLIST_H

class BaseLink;

class LList
{
public:
    LList();
    virtual ~LList();

    bool addToTail( BaseLink * element );
    bool addNode( BaseLink * element );
    bool delNode();
    bool del( BaseLink * element );

    int find( BaseLink * element );
    int count() const;
    BaseLink * current() const;

    void clear();
    void resetToHead();
    void resetToTail();
    void next();
    void prev();

private:
    struct Node {
        Node * next;
        Node * prev;
        BaseLink * element;
    };

    Node * _head;
    Node * _tail;
    Node * _curr;
    int _count;
};

#endif // LLIST_H
