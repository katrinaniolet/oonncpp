#include "inputnode.h"

InputNode::InputNode(int size) :
    BaseNode(size,size)
{
    for (int i=0; i<size; i++) {
        _error[i]=0.0;
        _value[i]=0.0;
    }
}

const char *InputNode::name() const
{
    return "INPUT_NODE";
}
