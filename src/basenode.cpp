#include <iomanip>
#include "basenode.h"
#include "baselink.h"

int BaseNode::_ticket=-1;

BaseNode::BaseNode( int vSize, int eSize ) :
    _id(++_ticket),
    _value(0),
    _valueSize(0),
    _error(0),
    _errorSize(0)
{
    if(vSize>0) {
        _valueSize = vSize;
        _value = new double[vSize];
    }
    for(int i=0; i<vSize; ++i)
        _value[i] = 0.0;

    if(eSize>0) {
        _errorSize = eSize;
        _error = new double[eSize];
    }
    for(int i=0; i<eSize; ++i)
        _error[i] = 0.0;

}

BaseNode::~BaseNode()
{
    if(_valueSize>0)
        delete[] _value;
    if(_errorSize>0)
        delete[] _error;
}

int BaseNode::id() const
{
    return _id;
}

const char *BaseNode::name() const
{
    return "BASE_NODE";
}

LList *BaseNode::inLinks()
{
    return &_inLinks;
}

LList *BaseNode::outLinks()
{
    return &_outLinks;
}

void BaseNode::run(int mode)
{
    UNUSED(mode)
}

void BaseNode::learn(int mode)
{
    UNUSED(mode)
}

void BaseNode::epoch(int code)
{
    UNUSED(code)
}

void BaseNode::load(std::ifstream &infile)
{
    infile >> _id;
    infile >> _valueSize;
    if(_value)
        delete[] _value;
    _value = new double[_valueSize];
    for(int i=0; i<_valueSize; ++i)
          infile >> _value[i];
    infile >> _errorSize;
    if(_error)
        delete[] _error;
    _error = new double[_errorSize];
    for(int i=0; i<_errorSize; ++i)
          infile >> _error[i];
}

void BaseNode::save(std::ofstream &outfile)
{
    outfile << std::setw(4) << _id << std::endl;
    outfile << _valueSize;
    for(int i=0; i<_valueSize; ++i)
          outfile << " " << std::setprecision(18) << _value[i];
    outfile << std::endl;
    outfile << _errorSize;
    for(int i=0; i<_errorSize; ++i)
          outfile << " " << std::setprecision(18) << _error[i];
    outfile << std::endl;
}

double BaseNode::value(int id) const
{
    return _value[id];
}

void BaseNode::setValue(double newVal, int id)
{
    _value[id] = newVal;
}

double BaseNode::error(int id) const
{
    return _error[id];
}

void BaseNode::setError(double newVal, int id)
{
    _error[id] = newVal;
}

void BaseNode::createLinkTo(BaseNode &toNode, BaseLink *link)
{
    _outLinks.addToTail(link);
    toNode.inLinks()->addToTail(link);
    link->setInNode(this, _id);
    link->setOutNode(&toNode, toNode.id());
}

void BaseNode::print(std::ofstream &out)
{
    out << "Node ID: " << _id << "   Node Name: " << name() << std::endl;
    out <<  "Value Set: ";
    for(int i=0; i<_valueSize; ++i)
        out << _value[i] << " ";
    out << std::endl;
    out <<  "Error Set: ";
    for(int i=0; i<_errorSize; ++i)
        out << _error[i] << " ";
    out << std::endl;

    _inLinks.resetToHead();
    for(int i=0; i<_inLinks.count(); ++i) {
        out << "   In Link ID : " << _inLinks.current()->id()
            << "  Link Name: " << _inLinks.current()->name()
            << "  Source Node: " << _inLinks.current()->inNode()->id()
            << "  Value Set: ";
         for(int j=0; j<_inLinks.current()->getSetSize(); ++j)
            out << _inLinks.current()->value(j) << " ";
         out << std::endl;
         _inLinks.next();
    }

    _outLinks.resetToHead();
    for(int i=0; i<_outLinks.count(); ++i) {
        out << "   Out Link ID: " << _outLinks.current()->id()
            << "  Link Name: " << _outLinks.current()->name()
            << "  Dest Node  : " << _outLinks.current()->outNode()->id()
            << "  Value Set: ";
        for (int j=0; j<_outLinks.current()->getSetSize(); ++j)
            out << _outLinks.current()->value(j) << " ";
        out << std::endl;
        _outLinks.next();
    }
    out << std::endl;
}
